import fs from 'fs'
import fspromise from 'fs/promises'
import 'core-js/stable'
import 'regenerator-runtime/runtime'

export const login = (nama, password) => {
    fspromise.readFile('data.json')
    .then(data => {
        let realData = JSON.parse(data)
        let indexChange = realData.findIndex(item => item.name == nama && item.password == password)
        realData.splice(indexChange[3], 1, true)
        return fspromise.writeFile('data.json', JSON.stringify(realData))
    })
    .then(() =>console.log("Berhasil Login !"))
    .catch(err => {
        console.log(err)
    })
}