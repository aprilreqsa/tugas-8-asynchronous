import fs from 'fs'
import fspromise from 'fs/promises'
import 'core-js/stable'
import 'regenerator-runtime/runtime'
export class Karyawan {
    constructor(name,password,role, isLogin = false){
        this.name = name
        this.password = password
        this.role = role
        this.isLogin = isLogin
    }
    static register(name,password,role,isLogin,callback){
        let newKaryawan = new this(name,password,role,isLogin)
        fs.readFile('data.json',(err, data)=> {
            if (err) callback(err, null)
            let realData = JSON.parse(data)
            let allKaryawan = realData.karyawan
            allKaryawan.push(newKaryawan)
            let newData = {
                ...realData,
                 karyawan: allKaryawan
            }
        
        fs.writeFile('data.json',JSON.stringify(newData),(err)=>{
            if (err) callback(err)
            callback(null, "Berhasil Register")
            })
        })

    }
    
     
}
